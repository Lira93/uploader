<?php

namespace App\Controller;

use App\Service\AmazonS3Service;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Dictionary\ImageDictionary;

class UploadController extends AbstractController
{
    private $amazonS3Service;
    
    public function __construct(AmazonS3Service $amazonS3Service)
    {
        $this->amazonS3Service = $amazonS3Service;
    }

    /**
     * @Route("/doUpload", name="do-upload")
     * @param Request $request
     * @param string $uploadDir
     *
     * @return Response
     */
    public function index(Request $request): Response
    {
        $token = $request->get("token");

        if (!$this->isCsrfTokenValid('upload', $token))
        {
            $logger->info("CSRF failure");

            return new Response("Operation not allowed",  Response::HTTP_BAD_REQUEST,
                                ['content-type' => 'text/plain']);
        }

        $file = $request->files->get('myfile');
        if (empty($file))
        {
            return new Response("No file specified",
                                Response::HTTP_UNPROCESSABLE_ENTITY, ['content-type' => 'text/plain']);
        }

        $this->validMimeType($file->getClientMimeType());
        $filename = $file->getClientOriginalName();
        list($width, $height) = getimagesize($file);
        $this->validSizeImage($width, $height);
        $this->amazonS3Service->upload($filename, $file->getPathName());

        return new Response("File uploaded",  Response::HTTP_OK,
                            ['content-type' => 'text/plain']);
    }

    private function validSizeImage($sizeWidth, $sizeHeight): Response
    {
        if($sizeWidth >= ImageDictionary::IMAGE_SIZE && $sizeHeight >= ImageDictionary::IMAGE_SIZE)
        {
            return new Response("No file specified",
                                Response::HTTP_UNPROCESSABLE_ENTITY, ['content-type' => 'text/plain']);
        }
    }

    private function validMimeType($clientMimeType)
    {
        if (!in_array($clientMimeType, ImageDictionary::MIME_TYPES)) {
            throw new \InvalidArgumentException(sprintf('Files of type %s are not allowed.', $file->getClientMimeType()));
        }
    }
}