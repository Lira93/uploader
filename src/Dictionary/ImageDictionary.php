<?php


namespace App\Dictionary;

/**
 * @name    Image - parametry dotyczace zdjecia
 * @author Krystian Liris <krystianiko@gmail.com>
 */
class ImageDictionary
{
    public const IMAGE_JPG = 'image/jpeg';
    public const IMAGE_PNG = 'image/png';
    public const IMAGE_GIF = 'image/gif';
    public const IMAGE_PDF = 'application/pdf';

    public const IMAGE_SIZE = 150;

    public const MIME_TYPES = [
        self::IMAGE_JPG,
        self::IMAGE_PNG,
        self::IMAGE_GIF,
        self::IMAGE_PDF
    ];

}